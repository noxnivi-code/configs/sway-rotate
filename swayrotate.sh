#!/usr/bin/bash
#
SCREEN="DSI-1"
TOUCHSCREEN="1267:8870:ELAN22A6:00_04f3:22A6"
STYLUS="1267:8870:ELAN22A6:00_04F3:22A6_Stylus"

swaymsg input "$TOUCHSCREEN" map_to_output "$SCREEN"
swaymsg input "$STYLUS" map_to_output "$SCREEN"

case $1 in
	"landscape")
		swaymsg output $SCREEN transform 90
		;;
	"portrait")
		swaymsg output $SCREEN transform 0
		;;
esac


